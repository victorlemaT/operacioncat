import requests
import datetime

def CierreOperacion(numero,URL):

    ano = datetime.date.today().strftime("%Y-%m-%d")
    min = datetime.datetime.now().strftime("%H")
    fecha = datetime.datetime.now().strftime("%M:%S.9754957+00:00")
    fechafinal = ano + "T" + str(int(min) + 1) + ":" + fecha
    url = str(URL)+"/ESign/esignservices/esendexpn/messagereceived"

    payload = "<InboundMessage>\n<Id>6eb2a830-595b-42e5-99a3-57d87e63db36</Id>\n<MessageId>607bc5ed-0e33-4491-88f5-b7c3982d5bdc</MessageId>\n<AccountId>f6462f9d-87a2-4cac-97a6-5a2d9cb81997</AccountId>\n<MessageText>Prueba</MessageText>\n<From>34"+str(numero)+"</From>\n<To>34637283012</To>\n<ReceivedAt>"+str(fechafinal)+"</ReceivedAt>\n</InboundMessage>"
    headers = {
        'Content-Type': "application/xml",
        'cache-control': "no-cache",
        'Postman-Token': "23ec67a8-0bbb-4143-8883-46100d100eaa"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    #print(response.text)
    return response.status_code

def CreacionOperacion(token,numero,URL):
    #soporte
    #793:SoportePortugal",188:"Soporte2",176:"Soporte1"

    url = str(URL)+"/repomanager/rest/v2/operation/create/notification/json/176"

    payload = "{\n  \"signers\": [\n    {\n      \"signerData\": [\n        {\n          \"identifier\": \"operationid\",\n          \"value\": \"prueba1\",\n          \"authentication\": \"N\",\n          \"label\": \"CUPS\",\n          \"resumen\": \"Y\",\n          \"order\": 1,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"company_name\",\n          \"value\": \"ENDESA ENERGÍA, S.A.U. (CIF/NIE A81948077)\",\n          \"authentication\": \"N\",\n          \"label\": \"Razón social\",\n          \"resumen\": \"Y\",\n          \"order\": 2,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"cellphone\",\n          \"value\": \"+34"+str(numero)+"\",\n          \"authentication\": \"N\",\n          \"label\": \"Teléfono\",\n          \"resumen\": \"Y\",\n          \"order\": 3,\n          \"actions\": [\n            {\n              \"className\": \"com.galeon.docsmanager.additionalactions.CancelOldOperationsAddActions\",\n              \"identifier\": \"cancelOldOperations\",\n              \"step\": \"AFTER-CREATION\"\n            }\n          ]\n        },\n        {\n          \"identifier\": \"textnotification\",\n          \"value\": \"Prueba Soporte 1 \",\n          \"authentication\": \"N\",\n          \"label\": \"SMS\",\n          \"resumen\": \"Y\",\n          \"order\": 101,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"numbersms\",\n          \"value\": \"1\",\n          \"authentication\": \"N\",\n          \"label\": \"Número de SMS\",\n          \"resumen\": \"Y\",\n          \"order\": 102,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"templateuuid\",\n          \"value\": \"f5baa3a8-834e-4116-804f-c309ae4df27c\",\n          \"authentication\": \"N\",\n          \"label\": \"Identificador plantilla\",\n          \"resumen\": \"Y\",\n          \"order\": 103,\n          \"actions\": []\n        }\n      ]\n    }\n  ],\n  \"actions\": [\n    {\n      \"className\": \"com.galeon.docsmanager.additionalactions.CancelOldOperationsAddActions\",\n      \"identifier\": \"cancelOldOperations\",\n      \"step\": \"AFTER-CREATION\"\n    }\n  ],\n  \"operationData\": [\n    {\n      \"identifier\": \"operationid\",\n      \"value\": \"prueba\",\n      \"authentication\": \"N\",\n      \"label\": \"CUPS\",\n      \"resumen\": \"Y\",\n      \"order\": 1,\n      \"actions\": []\n    },\n    {\n      \"identifier\": \"opClass\",\n      \"value\": \"794\",\n      \"authentication\": \"N\",\n      \"label\": \"opClass\",\n      \"resumen\": \"N\",\n      \"order\": 100,\n      \"actions\": []\n    }\n  ]\n}"
    headers = {
        'Authorization': token,
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "3c4d88a6-3f20-48e5-95c7-77488c91b8a2"
        }

    response = requests.request("POST", url, data=payload, headers=headers)
    #if response == 401 or response == 404:
     #   return response.status_code
    #elif response == 404:
    #else:
    return response.status_code
def CreacionOperacionPd(token,numero,URL):
    #portugal

    url = str(URL)+"/repomanager/rest/v2/operation/create/notification/json/194"

    payload = "{\n  \"signers\": [\n    {\n      \"signerData\": [\n        {\n          \"identifier\": \"operationid\",\n          \"value\": \"SoportePortugal\",\n          \"authentication\": \"N\",\n          \"label\": \"CUPS\",\n          \"resumen\": \"Y\",\n          \"order\": 1,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"company_name\",\n          \"value\": \"ENDESA ENERGÍA, S.A.U. (CIF/NIE A81948077)\",\n          \"authentication\": \"N\",\n          \"label\": \"Razón social\",\n          \"resumen\": \"Y\",\n          \"order\": 2,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"cellphone\",\n          \"value\": \"+34"+str(numero)+"\",\n          \"authentication\": \"N\",\n          \"label\": \"Teléfono\",\n          \"resumen\": \"Y\",\n          \"order\": 3,\n          \"actions\": [\n            {\n              \"className\": \"com.galeon.docsmanager.additionalactions.CancelOldOperationsAddActions\",\n              \"identifier\": \"cancelOldOperations\",\n              \"step\": \"AFTER-CREATION\"\n            }\n          ]\n        },\n        {\n          \"identifier\": \"textnotification\",\n          \"value\": \"Prueba Soporte Portugal\",\n          \"authentication\": \"N\",\n          \"label\": \"SMS\",\n          \"resumen\": \"Y\",\n          \"order\": 101,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"numbersms\",\n          \"value\": \"1\",\n          \"authentication\": \"N\",\n          \"label\": \"Número de SMS\",\n          \"resumen\": \"Y\",\n          \"order\": 102,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"templateuuid\",\n          \"value\": \"c08b5e12-3c89-4e6e-9b60-baad83b0a666\",\n          \"authentication\": \"N\",\n          \"label\": \"Identificador plantilla\",\n          \"resumen\": \"Y\",\n          \"order\": 103,\n          \"actions\": []\n        }\n      ]\n    }\n  ],\n  \"actions\": [\n    {\n      \"className\": \"com.galeon.docsmanager.additionalactions.CancelOldOperationsAddActions\",\n      \"identifier\": \"cancelOldOperations\",\n      \"step\": \"AFTER-CREATION\"\n    }\n  ],\n  \"operationData\": [\n    {\n      \"identifier\": \"operationid\",\n      \"value\": \"SoportePortugal\",\n      \"authentication\": \"N\",\n      \"label\": \"CUPS\",\n      \"resumen\": \"Y\",\n      \"order\": 1,\n      \"actions\": []\n    },\n    {\n      \"identifier\": \"opClass\",\n      \"value\": \"793\",\n      \"authentication\": \"N\",\n      \"label\": \"opClass\",\n      \"resumen\": \"N\",\n      \"order\": 100,\n      \"actions\": []\n    }\n  ]\n}"
    headers = {
        'Authorization': token,
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "f529de6f-faab-4e5b-9e61-b0622922e9f0"
    }

    response = requests.request("POST", url, data=payload, headers=headers)
    return response.status_code

def CreacionOperacionSd(token,numero,URL):
    #soporte2


    url = str(URL)+"/repomanager/rest/v2/operation/create/notification/json/188"

    payload = "{\n  \"signers\": [\n    {\n      \"signerData\": [\n        {\n          \"identifier\": \"operationid\",\n          \"value\": \"PruebaSoporte2\",\n          \"authentication\": \"N\",\n          \"label\": \"CUPS\",\n          \"resumen\": \"Y\",\n          \"order\": 1,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"company_name\",\n          \"value\": \"ENDESA ENERGÍA, S.A.U. (CIF/NIE A81948077)\",\n          \"authentication\": \"N\",\n          \"label\": \"Razón social\",\n          \"resumen\": \"Y\",\n          \"order\": 2,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"cellphone\",\n          \"value\": \"+34"+str(numero)+"\",\n          \"authentication\": \"N\",\n          \"label\": \"Teléfono\",\n          \"resumen\": \"Y\",\n          \"order\": 3,\n          \"actions\": [\n            {\n              \"className\": \"com.galeon.docsmanager.additionalactions.CancelOldOperationsAddActions\",\n              \"identifier\": \"cancelOldOperations\",\n              \"step\": \"AFTER-CREATION\"\n            }\n          ]\n        },\n        {\n          \"identifier\": \"textnotification\",\n          \"value\": \"Prueba Soporte 2\",\n          \"authentication\": \"N\",\n          \"label\": \"SMS\",\n          \"resumen\": \"Y\",\n          \"order\": 101,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"numbersms\",\n          \"value\": \"1\",\n          \"authentication\": \"N\",\n          \"label\": \"Número de SMS\",\n          \"resumen\": \"Y\",\n          \"order\": 102,\n          \"actions\": []\n        },\n        {\n          \"identifier\": \"templateuuid\",\n          \"value\": \"5014e1dd-ce9a-428a-8d74-8a9125484c1d\",\n          \"authentication\": \"N\",\n          \"label\": \"Identificador plantilla\",\n          \"resumen\": \"Y\",\n          \"order\": 103,\n          \"actions\": []\n        }\n      ]\n    }\n  ],\n  \"actions\": [\n    {\n      \"className\": \"com.galeon.docsmanager.additionalactions.CancelOldOperationsAddActions\",\n      \"identifier\": \"cancelOldOperations\",\n      \"step\": \"AFTER-CREATION\"\n    }\n  ],\n  \"operationData\": [\n    {\n      \"identifier\": \"operationid\",\n      \"value\": \"PruebaSoporte2\",\n      \"authentication\": \"N\",\n      \"label\": \"CUPS\",\n      \"resumen\": \"Y\",\n      \"order\": 1,\n      \"actions\": []\n    },\n    {\n      \"identifier\": \"opClass\",\n      \"value\": \"793\",\n      \"authentication\": \"N\",\n      \"label\": \"opClass\",\n      \"resumen\": \"N\",\n      \"order\": 100,\n      \"actions\": []\n    }\n  ]\n}"
    headers = {
        'Authorization': token,
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "a223d9e7-ee70-44e0-b5e8-bac1aa9807be"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    return response.status_code

def tokensms(URL):

    url = str(URL)+"/repomanager/services/repository/login"

    payload = "{\n  \"username\":\"viviana\",\n  \"userpassword\": \"c7auWK31gFnqITWdiOgVh51IiKYBHhlemgAFMrJ33Yk=\"\n}\n"
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "53cfaaa9-8660-4154-841d-d438ff0b3f65"
                }

    response = requests.request("POST", url, data=payload, headers=headers)

    if response.status_code == 400:
        return response.status_code
    else:
        token = response.text.strip('"')
        return token

def Respuesta():

    try:
        numero = 689962547
        URL = "https://endesasms.grupocmc.es"
        token = tokensms(URL)
        CreacionOperacio = CreacionOperacion(token,numero,URL)
        CierreOperacionSMS = CierreOperacion(numero,URL)
        CreacionOperacionP = CreacionOperacionPd(token,numero,URL)
        CierreOperacionSMS = CierreOperacion(numero,URL)
        CreacionOperacionS = CreacionOperacionSd(token,numero,URL)
        CierreOperacionSMS = CierreOperacion(numero,URL)
        return "Entorno EndesaSMS;Correcto"

    except OSError as token:
        return "Entorno EndesaSMS;Error_token"
    except OSError as CreacionOperacio:
        return "Entorno EndesaSMS;Error Creacion"
    except OSError as CierreOperacionSMS:
        return "Entorno EndesaSMS;Error Cierre"
