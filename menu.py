#!/usr/bin/python
# -*- coding: utf-8 -*-
#Creacion de una operaccion
import requests
import json
import string
import re



def tokenCAt():
    url = "https://endesacat.grupocmc.es/repomanager/services/repository/login"

    payload = "{\n  \"username\":\"agente\",\n  \"userpassword\": \"vVBisNmeuTh9wXwA2BA7pY+2b2l9spn7wVdJ12k34uw=\"\n}\n"
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "53cfaaa9-8660-4154-841d-d438ff0b3f65"
                }

    response = requests.request("POST", url, data=payload, headers=headers)
    print("token", response.elapsed)
    return str(response.text)
def creacionoperacion(contrasena):
    token = contrasena.strip('"')
    url = "https://endesacat.grupocmc.es/repomanager/rest/v2/operation/create/json/1"

    payload = "{\r\n  \"signers\": [\r\n    {\r\n      \"signerData\": [\r\n        {\r\n          \"identifier\": \"email\",\r\n          \"resumen\": \"Y\",\r\n          \"value\": \"victor.lema@grupocmc.es\",\r\n          \"actions\": [\r\n            {\r\n              \"identifier\": \"sendFilesToSalesForce\",\r\n              \"step\": \"FIRMA-FINAL\",\r\n              \"className\": \"com.galeon.docsmanager.additionalactions.GutenbergAddActions\"\r\n            }\r\n          ],\r\n          \"authentication\": \"N\",\r\n          \"order\": 1\r\n        },\r\n        {\r\n          \"identifier\": \"nif\",\r\n          \"resumen\": \"Y\",\r\n          \"value\": \"54712672L\",\r\n          \"actions\": [],\r\n          \"authentication\": \"N\",\r\n          \"order\": 2\r\n        },\r\n        {\r\n          \"identifier\": \"name\",\r\n          \"resumen\": \"Y\",\r\n          \"value\": \"Viviana\",\r\n          \"actions\": [],\r\n          \"authentication\": \"N\",\r\n          \"order\": 3\r\n        },\r\n        {\r\n          \"identifier\": \"lastname\",\r\n          \"resumen\": \"Y\",\r\n          \"value\": \"Sandagorda\",\r\n          \"actions\": [],\r\n          \"authentication\": \"N\",\r\n          \"order\": 4\r\n        },\r\n        {\r\n          \"identifier\": \"cellphone\",\r\n          \"resumen\": \"Y\",\r\n          \"value\": \"+34686183047\",\r\n          \"actions\": [],\r\n          \"authentication\": \"N\",\r\n          \"order\": 5\r\n        },\r\n        {\r\n          \"identifier\": \"confgutenberg\",\r\n          \"resumen\": \"N\",\r\n          \"value\": \"[{\\\"Type\\\":\\\"SM\\\",\\\"MasInfo\\\":\\\"3-TESTCMC\\\",\\\"creador\\\":\\\"ESGVDGMN3347\\\",\\\"oficina\\\":\\\"Agente CAT Digitex Manizales\\\",\\\"cliente\\\":\\\"15131872B\\\",\\\"puntoSuministro\\\":\\\"ES0031405160031007QJ0F\\\",\\\"contacto\\\":\\\"\\\",\\\"pedido\\\":\\\"Test_CMC_20181019_001\\\",\\\"reclamacion\\\":\\\"\\\",\\\"solicitud\\\":\\\"1\\\",\\\"actividad\\\":\\\"\\\",\\\"idCliente\\\":\\\"7005325364\\\",\\\"lista\\\":\\\"GTB001\\\",\\\"operacion\\\":\\\"FirmaDigital\\\",\\\"canal\\\":\\\"REC CAT GRAB TOTAL\\\",\\\"tipoCanal\\\":\\\"No Presencial\\\"}]\",\r\n          \"actions\": [],\r\n          \"authentication\": \"N\",\r\n          \"order\": 6\r\n        }\r\n      ],\r\n      \"docs\": [\r\n        \"doc1\"\r\n      ],\r\n      \"level\": 1,\r\n      \"operationType\": \"WEB\",\r\n      \"signaturesType\": [\r\n        \"SIMPLE\"\r\n      ],\r\n      \"order\": 1\r\n    }\r\n  ],\r\n  \"documents\": {\r\n    \"doc1\": {\r\n      \"reference\": \"doc1\",\r\n      \"documentB64\": \"\",\r\n      \"signupload\": 0,\r\n      \"belongToPriorLevels\": false,\r\n      \"signmultiple\": 1,\r\n      \"block\": 1,\r\n      \"transferible\": 0,\r\n      \"documentName\": \"pruebaSinAdjunto\",\r\n      \"state\": 0,\r\n      \"mandatory\": 1,\r\n      \"getterpdf\": {\r\n        \"inputParams\": {\r\n          \"docBase64\": \"UEsDBBQACAgIAJtQV0sAAAAAAAAAAAAAAAALAAAAX3JlbHMvLnJlbHOtkk1LA0EMhu/9FUPu3Wwr\r\niMjO9iJCbyL1B4SZ7O7Qzgczaa3/3kEKulCKoMe8efPwHNJtzv6gTpyLi0HDqmlBcTDRujBqeNs9\r\nLx9g0y+6Vz6Q1EqZXCqq3oSiYRJJj4jFTOypNDFxqJshZk9SxzxiIrOnkXHdtveYfzKgnzHV1mrI\r\nW7sCtftI/Dc2ehayJIQmZl6mXK+zOC4VTnlk0WCjealx+Wo0lQx4XWj9e6E4DM7wUzRHz0GuefFZ\r\nOFi2t5UopVtGd/9pNG98y7zHbNFe4ovNosPZG/SfUEsHCOjQASPZAAAAPQIAAFBLAwQUAAgICACb\r\nUFdLAAAAAAAAAAAAAAAAEQAAAHdvcmQvZG9jdW1lbnQueG1spVTLbtswELz3KwTebUlNELhC5Bxi\r\ntOihqQG7H0BJlESE5BLLlRX360tarzQBCqO5aEXN7uzMiuT9w4tW0Umgk2Bylq4TFglTQiVNk7Nf\r\nx6+rDYsccVNxBUbk7Cwce9h+uu+zCspOC0ORZzAug5x1aDJXtkJzt9KyRHBQ06oEnUFdy1KMgY0V\r\nmLOWyGZxPBatwQrjsRpQc/JLbOKhZDf2ij8nyV2MQnHyel0rrZvYTv/qf9Jqyuuv6doDVhahFM75\r\nQWg19NVcmpkmTa4wHHjmCntN5wp5/6rl30J2A7gwuneUs4y1lzFO78Li+dLkDd+h5VYsbM3H2L4h\r\ndHZi0+U1bjXH586GiVn/RwupJJ0vxhdR6e3HVL2d2f/xhf2jy+x7YwB5ofxB8ERRUMe2/iwUUJ1D\r\ntJfHHi/hQGcloj47cZWzp+BasTggGBLiJY4F+A7rM9pOWx+iSkQWO1HwiKSFaPfzcR0yacgfmEKN\r\nEyWN1Wc7CzDihfa8EYME2xx+e8QfhjT9EsbdZ61/v9vcbKaEHxz9VyVqCkk3tyEHZdO+WraCV8If\r\n4yQsCOyC1AA0IwUQgV7ApqMRHFs9dfo4SK21p69EKedhhe2yR6DJR82VG02Qt7ST6O36y2DCFR6L\r\nAMfLIOLpB8XLrbX9A1BLBwhi3WSv6gEAAPoEAABQSwMEFAAICAgAm1BXSwAAAAAAAAAAAAAAAA8A\r\nAAB3b3JkL3N0eWxlcy54bWzFVe1O2zAU/b+niPy/pCA0VRUBsYxKbKibKHsAx7lpLRzb83Uo5R32\r\nVHuxXZtEK00Qn9r+JPG59vW955zYRye3tUpuwKE0OmP7e2OWgBamlHqZsR9Xs9GEJei5LrkyGjK2\r\nAWQnxx+O1lP0GwWY0HqN03XGVt7baZqiWEHNcc9Y0BSrjKu5p6FbpmvjSuuMAERKX6v0YDz+mNZc\r\natal2T/sJaqlcAZN5feEqVNTVVJATEXL98fxq1Zdglo8p5Cau+vGjiif5V4WUkm/icWwpBbT86U2\r\njheKuqV62DH1WhrxGSreKI9h6L67dtiO4mtmtMdkPeUopMzYhSzAUXqjkwU4WTEKrU41PhICjv4U\r\nJc/Y3HiTLLjGJP/yNVnkySUsG8VdmCUwYzMHEMIsDdviHcE3XGXs4LBDctzFFNfLDgMcnS0ebnm3\r\nGuXzABWypPpWcnQ+DwvTtrt0t2e7OwqvtSzNOicWnFH3i227eHt62qMzOok29xtLnFvu+NJxuwr1\r\nxNB5GUgh+VQUQ/Maul5aOPb4cxYlTp+u6H9rJowyrmuBN978Wykjqc8l/kwLXsAdL02P/K1QrKzg\r\nCOU3PaSNhlvf4XkDzpoSPGHmUemuAeycZtzzYLmQse0C6EeGwMY4lMkrD47OrYMxe6m+QY1hedvI\r\nO/yRkwEZJ29R4yF3u4J8MuUmuaLQk3q0NP3lVUkNl00486IfWySUO2FbtD8g/XCI9Nd2diHpmul1\r\nFNDBZgZMtHXYDFlgV6hXFwoboOuwV2rObbDPk9QPeB0bax3dihdE+bypyYn4iO2D0V9g+7455f0z\r\nx2efOK/laV7SRd1j6fcvHfF3Y+ktgndfePwHUEsHCCfXpUZ9AgAACAkAAFBLAwQUAAgICACbUFdL\r\nAAAAAAAAAAAAAAAAHAAAAHdvcmQvX3JlbHMvZG9jdW1lbnQueG1sLnJlbHOtkU0KwjAQhfeeIsze\r\nplUQkaZuRHAr9QAxnbbBNgnJKHp7A4paKOLC5fx97zEvX1/7jl3QB22NgCxJgaFRttKmEXAot9Ml\r\nrItJvsdOUlwJrXaBxRsTBLREbsV5UC32MiTWoYmT2vpeUix9w51UJ9kgn6XpgvtPBhQDJttVAvyu\r\nyoCVN4e/sG1da4Ubq849GhqR4IFuHYZIlL5BEvCok8gBPi4/+6d8bQ2V8tjh28Gr9c3E/K8/QKKY\r\n5ecXnp2nhUnOB+EWd1BLBwj5LzDAxQAAABMCAABQSwMEFAAICAgAm1BXSwAAAAAAAAAAAAAAABEA\r\nAAB3b3JkL3NldHRpbmdzLnhtbEWOSw7CMAxE95wi8h4SWPCpSLvjAsABQmugUmJHsaHA6QkrlqM3\r\nM3r77pWieWKRkcnDcuHAIPU8jHTzcD4d5lswooGGEJnQwxsFuna2nxpB1doSUx9ImsnDXTU31kp/\r\nxxRkwRmpsiuXFLTGcrMTlyEX7lGkTlO0K+fWNoWRoK2XH+ZkpiZj6ZG06jgH9gcGvIZH1FO4HJVz\r\nrTxD9LBxux+2f5f2C1BLBwh21Y6tpQAAANAAAABQSwMEFAAICAgAm1BXSwAAAAAAAAAAAAAAABIA\r\nAAB3b3JkL2ZvbnRUYWJsZS54bWytUEFOwzAQvPMKy3fqtAeEoqYVEuKEeqDlAVtn01iy15HXJPT3\r\nuE4rIcihoN7sndmZ2VmuP50VPQY2nio5nxVSIGlfGzpU8n33cv8oBUegGqwnrOQRWa5Xd8uhbDxF\r\nFmmduBwq2cbYlUqxbtEBz3yHlLDGBwcxfcNBDT7UXfAamZO6s2pRFA/KgSF5lgnXyPimMRqfvf5w\r\nSHEUCWghpgu4NR3L1TmdGEoCl0LvjEMWGxzEm3dAmaBbCIwnTg+2kkUhVd4DZ+zxMg2ZnoHORN1e\r\n5j0EA3uLJ0iNZr9Mt0e393bSa3Frr6dEmbaaPIsHw/xPq1ezx5DLFlsMpsmuYOMmoRedn32rqWTz\r\nW5fwPRkQTwUbe7o+zp+KOj949QVQSwcIwcfZCB0BAABVAwAAUEsDBBQACAgIAJtQV0sAAAAAAAAA\r\nAAAAAAAQAAAAZG9jUHJvcHMvYXBwLnhtbJ2RS2vDMBCE7/0VRuQayzaJCUVW6IOeAg3UbXoLqrSx\r\nVawH0iYk/75qU1yfe5vRLN9IK7Y+myE7QYja2YaUeUEysNIpbbuGvLZP8xXJIgqrxOAsNOQCkaz5\r\nDdsG5yGghpglgo0N6RH9LaVR9mBEzFNsU3JwwQhMNnTUHQ5awqOTRwMWaVUUNYUzglWg5n4Ekivx\r\n9oT/hSonv+8X39qLTzzOWjB+EAic0T/ZOhRDqw3wIh2Pht15P2gpMG2Eb/RHgOefCrrMy7zOq9lG\r\n2+N5/76q9/Uimwzs0xM+QSItC1PM7o96UPOK0SmObUUHkZeMXgXbuaAiXzJ6FeyhF0FITP/BqwWj\r\nEzuJdhr7Fy9kAlSr6dAkSE1BdEH4/rdudMmMq+ZfUEsHCFbgOx0iAQAAAAIAAFBLAwQUAAgICACb\r\nUFdLAAAAAAAAAAAAAAAAEQAAAGRvY1Byb3BzL2NvcmUueG1slVLLboMwELz3K5DvYEzShxAQqY+c\r\nGqlSU7XqzbU3xC02lu2E5O9rIJC0zaW3nZ3x7MvZbCerYAvGilrliEQxCkCxmgtV5uhlOQ9vUGAd\r\nVZxWtYIc7cGiWXGRMZ2y2sCTqTUYJ8AG3kjZlOkcrZ3TKcaWrUFSG3mF8uSqNpI6D02JNWVftASc\r\nxPEVluAop47i1jDUoyM6WHI2WuqNqToDzjBUIEE5i0lE8FHrwEh79kHHnCilcHsNZ6UDOap3VozC\r\npmmiZtJJff8Evy0en7tRQ6HaVTFARXZoJGUGqAMeeIO0Lzcwr5O7++UcFUlMrkMSh8lkSZI0nqZk\r\n+p7hX+9bwz6uTdGyR+BjDpYZoZ2/YU/+SHhcUVVu/MILsOHDcycZU+0pK2rdwh99JYDf7r3HmdzQ\r\nkTzk/jPS5elIg0FX2cBWtH+vIF3REbZd283HJzDXjzQCHzvhKujTQ/jnPxbfUEsHCBpQrkpfAQAA\r\n2wIAAFBLAwQUAAgICACbUFdLAAAAAAAAAAAAAAAAEwAAAFtDb250ZW50X1R5cGVzXS54bWy9lD9P\r\nwzAQxfd+isgrShwYEEJJOyAxQocwI2NfEov4j3ymtN+ec1siBKgRtLBYsnzv/d7dSa4WazNkKwio\r\nna3ZeVGyDKx0StuuZg/NbX7FFvNZ1Ww8YEa1FmvWx+ivOUfZgxFYOA+WXloXjIh0DR33Qj6LDvhF\r\nWV5y6WwEG/OYPNi8uidc0AqypQjxThioGX8MMCAv0smym50gMWsmvB+0FJHy8ZVVn2j5npSU2xrs\r\ntcczKmD8e9KrC4orJ18MIYpU+COea1stYdQnNx+cBESamBmK8cUIbSdzYNwMgKdPsfOdxO+m/nEY\r\n/7YBhBgp61/0vneejNAStBFPA5w+w2h9KATJl8F55AQ7OgKsSalA5ZTDQ4j68PpHtnThF/2/7zyp\r\nvxJnFd/+F/M3UEsHCMEiGkkqAQAAXgQAAFBLAQIUABQACAgIAJtQV0vo0AEj2QAAAD0CAAALAAAA\r\nAAAAAAAAAAAAAAAAAABfcmVscy8ucmVsc1BLAQIUABQACAgIAJtQV0ti3WSv6gEAAPoEAAARAAAA\r\nAAAAAAAAAAAAABIBAAB3b3JkL2RvY3VtZW50LnhtbFBLAQIUABQACAgIAJtQV0sn16VGfQIAAAgJ\r\nAAAPAAAAAAAAAAAAAAAAADsDAAB3b3JkL3N0eWxlcy54bWxQSwECFAAUAAgICACbUFdL+S8wwMUA\r\nAAATAgAAHAAAAAAAAAAAAAAAAAD1BQAAd29yZC9fcmVscy9kb2N1bWVudC54bWwucmVsc1BLAQIU\r\nABQACAgIAJtQV0t21Y6tpQAAANAAAAARAAAAAAAAAAAAAAAAAAQHAAB3b3JkL3NldHRpbmdzLnht\r\nbFBLAQIUABQACAgIAJtQV0vBx9kIHQEAAFUDAAASAAAAAAAAAAAAAAAAAOgHAAB3b3JkL2ZvbnRU\r\nYWJsZS54bWxQSwECFAAUAAgICACbUFdLVuA7HSIBAAAAAgAAEAAAAAAAAAAAAAAAAABFCQAAZG9j\r\nUHJvcHMvYXBwLnhtbFBLAQIUABQACAgIAJtQV0saUK5KXwEAANsCAAARAAAAAAAAAAAAAAAAAKUK\r\nAABkb2NQcm9wcy9jb3JlLnhtbFBLAQIUABQACAgIAJtQV0vBIhpJKgEAAF4EAAATAAAAAAAAAAAA\r\nAAAAAEMMAABbQ29udGVudF9UeXBlc10ueG1sUEsFBgAAAAAJAAkAPAIAAK4NAAAAAA==\"\r\n        },\r\n        \"methodName\": \"word2PdfConverter\",\r\n        \"className\": \"com.galeon.docsmanager.additionalactions.Word2PdfConverterAddActions\"\r\n      },\r\n      \"order\": 1\r\n    }\r\n  },\r\n  \"operationData\": [\r\n    {\r\n      \"identifier\": \"opClass\",\r\n      \"resumen\": \"Y\",\r\n      \"value\": \"7771\",\r\n      \"authentication\": \"N\",\r\n      \"order\": 1\r\n    },\r\n    {\r\n      \"identifier\": \"domainid\",\r\n      \"resumen\": \"Y\",\r\n      \"value\": \"1\",\r\n      \"authentication\": \"N\",\r\n      \"order\": 1\r\n    },\r\n    {\r\n      \"identifier\": \"operationid\",\r\n      \"resumen\": \"Y\",\r\n      \"value\": \"Test_CMC_20181019_001\",\r\n      \"authentication\": \"N\",\r\n      \"order\": 1\r\n    }\r\n  ]\r\n}"
    headers = {
        'Content-Type': "application/json",
        'Authorization': token,
        'cache-control': "no-cache",
        'Postman-Token': "961f5750-79f8-4dc6-b05b-04bfba54b1cd"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    print("creacion operacion",response.elapsed)
    return response.text

def EnvioOtp(uuids):
    # Se envia otp
    uuidsb = str(uuids).replace("[", "")
    uuidsC = str(uuidsb).replace("]", "")
    uuidsf = str(uuidsC).replace("'", "")

    url = "https://endesacat.grupocmc.es/ESign/esignservices/esign/checksecondotp"

    payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"operationuuid\"\r\n\r\n"+uuidsf+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
    headers = {
        'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        'cache-control': "no-cache",
        'Postman-Token': "1d44333c-fe13-47a2-8061-0b3f20be1378"
    }

    response = requests.request("POST", url, data=payload, headers=headers)
    print("envio otp",response.elapsed)
    return response.status_code

def OTPID(uuids,contrasena):

    uuidsb = str(uuids).replace("[", "")
    uuidsC = str(uuidsb).replace("]", "")
    uuidsf = str(uuidsC).replace("'", "")
    token = contrasena.strip('"')

    # acomulamos otp y id de la operacion
    url = "https://endesacat.grupocmc.es/repomanager/services/operations/getoperationbyuuid/1/"+uuidsf+""

    payload = ""
    headers = {
        'Authorization': token,
        'cache-control': "no-cache",
        'Postman-Token': "ddbea0ad-ede9-4e7b-8838-338a50b4b1cc"
    }

    response = requests.request("GET", url, data=payload, headers=headers)
    print(response.elapsed)
    p = response.text

    pppt = json.loads(p)
    otp = pppt['notifications'][1]['content']['body']

    tota = len(otp)
    otpi = otp[114:119]
    #print(otpi)

    id = pppt['events'][0]["operationId"]
    #print(id)
    datos = str(otpi)+","+str(id)
    return datos





def cierreOperacion(datosfirma):
    #Nos Falta , id , otp  ,     #Nos Falta , id , otp  , #
    d =datosFtirma.split(",")
    url = "https://endesacat.grupocmc.es/ESign/esignservices/esign/verifyotp"

    payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"id\"\r\n\r\n"+d[1]+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"otp\"\r\n\r\n"+d[0]+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"signatureType\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
    headers = {
        'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        'cache-control': "no-cache",
        'Postman-Token': "fce9d320-46a2-40fb-b686-9c56e9b23516"
    }

    response = requests.request("POST", url, data=payload, headers=headers)
    print("Coerre operacion",response.elapsed)
    return response.status_code


#nos da el toquen de la operacion
contrasena = tokenCAt()
#print(contrasena)
#se envia a la creacion
OperacionCat = creacionoperacion(contrasena)
#print(OperacionCat)
setuuids = json.loads(OperacionCat)
#nos muestra el uuids de la operacion
uuids = setuuids['uuids']
#print(uuids)
EnvioOtp = EnvioOtp(uuids)
#print(EnvioOtp)

datosFtirma = OTPID(uuids,contrasena)
#print(datosFtirma)

firma = cierreOperacion(datosFtirma)

print(firma)
if EnvioOtp == 202:
    print("otp envidad ")
    if firma == 503:
        print("documento firmado sin envio error 503")
    else:
        print("documento Digitalizado No hay error ")
else:
    print("No se envio la otp es un error de mensaje")