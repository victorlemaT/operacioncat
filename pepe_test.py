import requests

def CreacionToken(URL):

    url = str(URL)+"/repomanager/services/repository/login"

    payload = "{\"username\":\"agimeno.test\",\"userpassword\":\"c7auWK31gFnqITWdiOgVh51IiKYBHhlemgAFMrJ33Yk=\"}"
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "381c6113-1fda-4d07-84b3-7b1ce6fbbdfe"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.elapsed)
    if str(response.elapsed) <= str("0:00:00.144658"):
        print("rtarda mucho ")
    else:
        token = response.text.strip('"')

    return str(token)

def CreacionCampanaPepeper(URL):
    #Creacion de una operacion de firma PinWeb.
    url = str(URL)+"/O2Bridge/services/pepper/newoperationmultilevel"

    payload = "{\r\n  \"idOperation\": \"FIRMAPINWEB-TEST-IUBAGO\",\r\n  \"creatorId\": \"POSTMAN\",\r\n  \"domainId\": \"5\",\r\n  \"operationTypeId\": \"17\",\r\n  \"levelsdata\" : [\r\n    {\r\n      \"levelOrder\": 1,\r\n      \"customers\": [\r\n        {\r\n          \"customerId\": \"24564201V\",\r\n        \"customerEmail\": \"victor.lemat@gmail.com\",\r\n        \"customerPhone\": \"686183047\",\r\n        \"customerName\": \"Iñigo\",\r\n        \"customerLastname\": \"Ubago\",\r\n        \"customerAttributes\": [\r\n          {\r\n            \"key\": \"codigoag\",\r\n            \"value\": \"FIRMAPINWEB-TEST-IUBAGO\",\r\n            \"visibleLabel\": \"Y\"\r\n         },\r\n          {\r\n            \"key\": \"email_documentacion\",\r\n            \"value\": \"victor.lema@grupocmc.es\",\r\n            \"visibleLabel\": \"Y\"\r\n         }\r\n        ],\r\n        \"operationType\": \"WEB\",\r\n        \"signatureType\" : \"1\",\r\n        \"resume2\": \"N\",\r\n        \"idiomadef\": \"es-es\",\r\n        \"conditionsVisibility\": \"Y\"\r\n        }\r\n      ]\r\n    }\r\n  ],\r\n  \"documents\": [\r\n    {\r\n      \"documentName\": \"Contrato\",\r\n      \"documentUrl\": \"https://dl.dropboxusercontent.com/s/4k6351g8f2gfaas/Prueba_146.pdf\",\r\n      \"documentType\": \"\",\r\n      \"signatureType\": \"\",\r\n      \"level\": \"\",\r\n      \"docunentLabel\": \"\",\r\n      \"documentUuid\": \"\"\r\n   }\r\n  ]\r\n}"
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "cfd194a9-9202-4114-8aa6-84a9faa65e55"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    print(response.text)
    CreacionPWF = response.json()
    codifo = CreacionPWF['firstuuid']
    return codifo

def EnvioOtp(Creacionpepper,URL):
    url = str(URL)+"/ESign/esignservices/esign/checksecondotp"

    payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"operationuuid\"\r\n\r\n"+str(Creacionpepper)+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
    headers = {
        'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        'cache-control': "no-cache",
        'Postman-Token': "01a3c2bb-ea95-45e6-8d4f-4dfa5ad630f5"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    return response.text


def DAtosUidOtp(Creacionpepper,token,URL):

    url = str(URL)+"/repomanager/services/operations/getoperationbyuuid/37/"+str(Creacionpepper)+""

    payload = ""
    headers = {
        'Authorization': token,
        'cache-control': "no-cache",
        'Postman-Token': "ab002b4a-011d-4776-8d42-11aacb519a3f"
    }

    response = requests.request("GET", url, data=payload, headers=headers)
    datosID = response.json()
    ID = datosID['id']
    OTP = datosID['notifications'][0]['content']['body']
    indice = len(OTP)
    print("dato para la otp",indice)
    inr = indice - 4
    datoIDOPT = str(ID)+","+str(OTP[inr:indice])

    return datoIDOPT

def CierreOPeracion(Datosuidotp,URL):
    date = Datosuidotp.split(",")

    url = str(URL)+"/ESign/esignservices/esign/signpepperop"

    payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"id\"\r\n\r\n"+date[0]+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"otp\"\r\n\r\n"+date[1]+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"signatureType\"\r\n\r\n5\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
    headers = {
        'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        'cache-control': "no-cache",
        'Postman-Token': "43a9cebe-88ad-4599-bc9c-0993a0f5d960"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    return response.status_code

def RespuestaFinal():

    try:
        URL = "https://o2certifytest.galeonsoftware.com"

        token = CreacionToken(URL)
        Creacionpepper = CreacionCampanaPepeper(URL)
        EnvioOTP = EnvioOtp(Creacionpepper,URL)
        Datosuidotp = DAtosUidOtp(Creacionpepper,token,URL)
        CierreOPERA = CierreOPeracion(Datosuidotp,URL)
        print("token;"+str(token))
        print("Creacionde la operacion : "+ Creacionpepper)
        print(EnvioOTP)
        print(Datosuidotp)


        #if CierreOPERA == 200:
          #  return "Entorno Pepper;Correcto"
        #elif CierreOPERA == 503:
            #return "Pepper;CorrectoNoDigitalizado"
         #   return "Entorno Pepper;Correcto_con_Error"

    except OSError as CierreOPERA:
        return "Error PPEPPEr;Error"
